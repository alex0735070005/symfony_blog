<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Services\UserService;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
   
    public function login(AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }
    
    public function registration(Request $req, UserPasswordEncoderInterface $passwordEncoder)
    {
        $lastUsername = '';
        
        $isRegister = true;
        
        if($req->isMethod('POST'))
        {
            $data = $req->request->all();         
            
            $em = $this->getDoctrine()->getManager();
            
            $UserService = new UserService($passwordEncoder);
            
            $isRegister = $UserService->registr($em, $data);
            
            $lastUsername = $data['email'];
            
            if($isRegister){
                return $this->render('security/success.html.twig');
            }
        }
        
        return $this->render('security/registration.html.twig', ['last_username' => $lastUsername, 'isRegister' => $isRegister]);
    }
    
}
