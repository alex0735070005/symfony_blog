<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\News;

class NewsController extends AbstractController
{
    public function showList()
    {
        $em = $this->getDoctrine()->getManager();            
        $NewsRepo = $em->getRepository(News::class);
        
        $news_list = $NewsRepo->getNews();
        
        return $this->render('news/list.html.twig', [
            'news_list' => $news_list
        ]);
    }
    
    
    public function add(Request $req)
    {
        if($req->isMethod('POST'))
        {
            $data = $req->request->all();         
            
            $em = $this->getDoctrine()->getManager();
            
            $NewsRepo = $em->getRepository(News::class);
            
            
            if(!$NewsRepo->addNews($data)){
                return $this->render('news/exists.html.twig');
            }
                        
            return $this->render('news/success.html.twig');
        }   
        
        return $this->render('news/add.html.twig');
    }
    
    public function show($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $News = $em->find(News::class, $id);
                
        return $this->render('news/show.html.twig', [
            'News' => $News
        ]);
    }
    
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $News = $em->find(News::class, $id);
        
        $title = $News->getTitle();
        
        $em->remove($News);
        
        $em->flush();
        
        return $this->render('news/delete.html.twig', [
            'title' => $title
        ]);
    }
}