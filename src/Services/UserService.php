<?php

namespace App\Services;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class UserService
{
    private $passwordEncoder;
    
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    
    public function registr(ObjectManager $manager, $userData)
    {
        if( empty($userData['email']) || empty($userData['password']) || $userData['password'] !== $userData['plain_password']){
            return false;
        }
        
        $User = new User();
        
        $User->setEmail($userData['email']);
        
        $User->setRoles(['ROLE_USER']);
        
        $User->setPassword($this->passwordEncoder->encodePassword(
            $User,
            $userData['password']
        ));

        
        $manager->persist($User);
        
        try {
            $manager->flush();
            return true;
        } 
        catch (\Doctrine\DBAL\DBALException $e) {
            return false;
        }
    }
}
