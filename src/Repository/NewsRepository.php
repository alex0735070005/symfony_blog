<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


class NewsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function addNews($data)
    {
        try {
            $em = $this->getEntityManager();

            $News = new News();

            $News->setTitle($data['title']);
            $News->setDescription($data['description']);

            $em->persist($News);
            $em->flush($News);
            return true;
        } 
        catch (\Doctrine\DBAL\DBALException $e) {
            return false;
        }
    }
    
    public function getNews()
    {
        
        $em = $this->getEntityManager();

        //$query = $em->createQuery('SELECT n.id, n.title, n.description, n.date_updated FROM App\Entity\News n');
        $query = $em->createQuery('SELECT n FROM App\Entity\News n ORDER BY n.id DESC');
        
        return $query->getResult();
      
    }
}
