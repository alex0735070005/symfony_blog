<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class UserFixtures extends Fixture
{
    private $passwordEncoder;
    
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    
    public function load(ObjectManager $manager)
    {
        $User = new User();
        
        $User->setEmail('admin2@gmail.com');
        
        $User->setRoles(['ROLE_ADMIN']);
        
        $User->setPassword($this->passwordEncoder->encodePassword(
            $User,
            '1111'
        ));

        
        $manager->persist($User);

        $manager->flush();
    }
}
